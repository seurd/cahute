.. _protocol-seven:

Protocol 7.00 -- Serial and USB protocol used by post fx-9860G calculators
==========================================================================

This protocol is used by calculators starting from the fx-9860G, published
in 2004, up to the current day.

.. note::

    fx-CG calculators and derivatives still use a derivative from Protocol
    7.00, although not directly, but hidden behind proprietary SCSI commands.
    See :ref:`protocol-ums` for more information.

.. toctree::
    :maxdepth: 1

    seven/specific-encodings
    seven/packet-format
    seven/flows
    seven/casio-commands
    seven/fxremote-commands
    seven/use-cases
    seven/hardware-identifiers

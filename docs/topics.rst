Discussion topics
=================

In this section, we will explore discussion topics regarding the project's
environment and design. These topics provide keys to understanding what is
happening in this project, and why it is happening.

.. toctree::
    :maxdepth: 2

    topics/picture-formats
    topics/protocols
    topics/usb-detection
    topics/logging

Project management
==================

These documents describe the decisions taken for the project to handle
collaboration efficiently.

.. toctree::

    project/governance
    project/versioning
    project/contribution-style
    project/coding-style

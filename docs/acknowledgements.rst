Acknowledgements
================

There have been many projects over the years about reversing and
reimplementing CASIO's shenannigans for using their calculators from
alternative OSes, or simply for fun or out of curiosity. Cahute couldn't
have been made without their research, implementations and in some cases,
documentation. This page is a little tribute to these works.

* Thanks to Tom Wheeley and Tom Lynn for their work on CaS and Caspro_
  and the `Casio Graphical Calculator Encyclopaedia`_.
* Thanks to the (now defunct) Graph100.com wiki, `saved here
  <Graph100.com Wiki_>`_ for historical purposes.
* Thanks to the team behind Casetta_ for their documentation on legacy
  protocols and file formats, which helped me navigate the subtleties more
  easily.
* Thanks to `Simon Lothar`_ and Andreas Bertheussen for their work on
  Protocol 7.00 and derivatives through fxReverse_ and xfer9860, and to
  Teamfx_ for `their additions <Teamfx additions_>`_.
* Thanks to the Cemetech community for their `Prizm Wiki`_, especially
  gbl08ma, BrandonWilson and amazonka.
* Thanks to Nessotrin_ for their work on UsbConnector_, which prompted me
  to work on a better version in the first place.

There are obviously plenty more people working on other connected aspects
(hardware, low-level system stuff), administering or moderating forums and
websites, maintaining communication with CASIO and other partners.
Quoting you all would take a substantial time, and I'd likely miss quite a lot
of you, but thank you all for your efforts!

.. _Simon Lothar:
    https://www.casiopeia.net/forum/memberlist.php?mode=viewprofile&u=10405
.. _Teamfx:
    https://www.casiopeia.net/forum/memberlist.php?mode=viewprofile&u=10504&sid=b1f4fb842b29e6f686d832a7e1117789
.. _Nessotrin:
    https://www.planet-casio.com/Fr/compte/voir_profil.php?membre=nessotrin

.. _Casio Graphical Calculator Encyclopaedia:
    https://serval.mythic-beasts.com/~tom/calcs/calcs/encyc/
.. _Graph100.com Wiki:
    https://bible.planet-casio.com/cakeisalie5/websaves/graph100.com/
.. _fxReverse:
    https://bible.planet-casio.com/simlo/fxreverse/fxReverse2.pdf
.. _Teamfx additions: https://bible.planet-casio.com/teamfx/
.. _Prizm Wiki: https://prizm.cemetech.net/
.. _UsbConnector:
    https://www.planet-casio.com/Fr/forums/topic13656-1-usbconnector
    -remplacement-de-fa124-multi-os.html

.. _Casetta: https://casetta.tuxfamily.org/
.. _Caspro:
    https://web.archive.org/web/20160504230033/
    http://www.spiderpixel.co.uk:80/caspro/

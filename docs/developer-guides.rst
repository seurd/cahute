Developer Guides
================

These sections describe specific problems that you may want to solve using
the Cahute C library directly.

.. toctree::
    :maxdepth: 1

    developer-guides/build
    developer-guides/detect-usb
    developer-guides/detect-serial
    developer-guides/open-usb-link

.. _protocol-cas50:

CAS50 -- Serial protocol with 50 bytes long packets
===================================================

This protocol is used on serial links with calculators from the late 1990s
era, and early 2000s era excluding AFX.

It is mostly used on serial links, with a default configuration of 9600 bauds,
no parity and 1 stop bit.

.. todo:: More information here.

.. toctree::
    :maxdepth: 2

    cas50/packet-format

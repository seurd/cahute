Cahute -- Communication and file format handling tools for CASIO calculators
============================================================================

Cahute is a library and set of command-line utilities to handle serial
and USB communication protocols and file formats related to CASIO calculators,
dating from the 1990s to today.

For more information, consult the following links:

* `Cahute documentation`_, for more information on the project and its context;
* `Issue tracker`_, and the `issue reporting guide`_;
* `Pending contributions`_, and the related `contributing guide`_;
* `Topic on Planète Casio`_ (*in French*).

.. _Cahute documentation: https://cahuteproject.org/
.. _Issue tracker: https://gitlab.com/cahuteproject/cahute/-/issues
.. _Issue reporting guide: https://cahuteproject.org/guides/report.html
.. _Pending contributions:
    https://gitlab.com/cahuteproject/cahute/-/merge_requests
.. _Contributing guide: https://cahuteproject.org/guides/contribute.html
.. _Topic on Planète Casio:
    https://www.planet-casio.com/Fr/forums/topic17699-1-cahute-pour-
    communiquer-efficacement-avec-sa-calculatrice-casio-sous-linux.html

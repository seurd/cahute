Cahute |version|
================

Cahute is a library and set of command-line utilities to handle serial
and USB communication protocols and file formats related to CASIO calculators,
dating from the 1990s to today.

The project is also present `on Gitlab <Cahute on Gitlab_>`_
and `on Planète Casio <Cahute on Planète Casio_>`_.
It is maintained by `Thomas Touhey`_.

The project's code and documentation contents are licensed under CeCILL_
version 2.1 as distributed by the CEA, CNRS and Inria on
`cecill.info <CeCILL_>`_.

This documentation is organized using `Diátaxis`_' structure.

.. toctree::
    :maxdepth: 3

    acknowledgements
    guides
    cli-guides
    developer-guides
    topics
    project
    cli
    headers

.. _Cahute on Gitlab: https://gitlab.com/cahuteproject/cahute
.. _Thomas Touhey: https://thomas.touhey.fr/
.. _CeCILL: http://www.cecill.info/licences.en.html
.. _Diátaxis: https://diataxis.fr/
.. _Cahute on Planète Casio:
    https://www.planet-casio.com/Fr/forums/topic17699-1-cahute-pour-
    communiquer-efficacement-avec-sa-calculatrice-casio-sous-linux.html

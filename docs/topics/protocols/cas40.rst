.. _protocol-cas40:

CAS40 -- Serial protocol with 40 bytes long packets
===================================================

This protocol is used on serial links with calculators from the early 1990s
era.

.. todo:: More information here.

.. toctree::
    :maxdepth: 2

    cas40/packet-format

.. _header-cahute-picture:

``<cahute/picture.h>`` -- Picture format related utilities for Cahute
=====================================================================

Macro definitions
-----------------

``CAHUTE_PICTURE_FORMAT_*`` are constants representing how a given
picture's data is encoded.

.. c:macro:: CAHUTE_PICTURE_FORMAT_1BIT_MONO

    Constant representing the :ref:`picture-format-1bit`.

.. c:macro:: CAHUTE_PICTURE_FORMAT_1BIT_DUAL

    Constant representing the :ref:`picture-format-2bit-dual`.

.. c:macro:: CAHUTE_PICTURE_FORMAT_4BIT_RGB_PACKED

    Constant representing the :ref:`picture-format-4bit-rgb-packed`.

.. c:macro:: CAHUTE_PICTURE_FORMAT_16BIT_R5G6B5

    Constant representing the :ref:`picture-format-r5g6b5`.
